
SRC_FILES = lib.asm dict.asm main.asm
OBJ_FILES = $(SRC_FILES:.asm=.o)

NASM = nasm
LD = ld

NASM_FLAGS = -f elf64
LD_FLAGS =

OUTPUT = main

all: $(OUTPUT)

%.o: %.asm
	$(NASM) $(NASM_FLAGS) $< -o $@

main.o: words.inc    
words.inc: colon.inc

$(OUTPUT): $(OBJ_FILES)
	$(LD) $(LD_FLAGS) -o $(OUTPUT) $(OBJ_FILES)

run: $(OUTPUT)
	./$(OUTPUT)

clean:
	rm -f $(OBJ_FILES) $(OUTPUT)

test:
	python3 test.py

.PHONY: all clean run
