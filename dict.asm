%include "lib.inc"

global find_word

section .text

find_word:
        .loop: 
               push rdi
               add rsi, 8
               push rsi
               call string_equals
               pop rsi
               pop rdi
               test rax, rax
               jne .success  
               sub rsi, 8  
               mov rsi, [rsi]
               test rsi, rsi
               jz .failure
               jmp .loop    
               
        .failure:
                  xor rax, rax
                  ret
                           
        .success:
                  mov rax, rsi   
                  ret  
                     
       