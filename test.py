import unittest
import subprocess

class TestDictionary(unittest.TestCase):
    
    def setUp(self):
        self.app_command = "./main"
        
    def run_dict_program(self, input_word):
        process = subprocess.Popen(
            self.app_command,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            shell=True
        )
        stdout, stderr = process.communicate(input=input_word)
        return stdout.strip(), stderr.strip(), process.returncode

    def test_first_word(self):
        expected_output = ("integrity", "", 0)
        self.assertEqual(self.run_dict_program("purple_word"), expected_output)

    def test_second_word(self):
        expected_output = ("nostalgia", "", 0)
        self.assertEqual(self.run_dict_program("blue_word"), expected_output)

    def test_third_word(self):
        expected_output = ("passion word", "", 0)
        self.assertEqual(self.run_dict_program("red_word"), expected_output)

    def test_word_not_found(self):
        expected_output = ("", "The word hasn't been found", 1)
        self.assertEqual(self.run_dict_program("xxx"), expected_output)

    def test_word_too_long(self):
        expected_output = ("", "The size of the string is too long", 1)
        self.assertEqual(self.run_dict_program("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"), expected_output)

if __name__ == "__main__":
    unittest.main()
