%define POINTER 0

%macro colon 2
   %ifid %2
      %2: dq POINTER 
   %else
      %error "Id ought to be the second argument"
   %endif

   %ifstr %1
      db %1, 0
   %else
      %error "String ought to be the first argument"
   %endif

   %define POINTER %2

%endmacro