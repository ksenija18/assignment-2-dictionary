global _start

%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUFFER_STORAGE 256


section .rodata
    msg_size:     db "The size of the string is too long", 0
    msg_not_found:      db "The word hasn't been found", 0


section .bss
    BUFFER: resb BUFFER_STORAGE

section .text

_start:
    mov rdx, BUFFER_STORAGE
    lea rsi, [BUFFER]
    syscall
    test rax, rax        
    cmp rax, BUFFER_STORAGE  
    je .size_error
    lea rdi, [BUFFER]
    mov rsi, purple_word 
    call find_word       
    je .not_found_error
    mov rdi, rax          
    push rdi             
    call string_length  
    pop rdi               
    add rdi, rax   
    inc rdi           
    call print_string
    xor rdi, rdi
    call exit

   .size_error:
    mov rdi, msg_size
    call print_error
    mov rdi, 1
    call  exit

  .not_found_error:
    mov rdi, msg_not_found
    call print_error
    mov rdi, 1
    call  exit
 


